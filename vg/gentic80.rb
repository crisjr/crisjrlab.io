def main
  carts = [
    "./insanity/",
    "./clouds/",
    "./voronoi/",
    "./points/",
    "./falling_squares/",
    "./spirals/",
    "./spirals2/",
    "./boids/",
    "./not_feeling_well/",
    "./mandelbrot/",
    "./julia/",
    "./wave_function_collapse/",
    "./rainbow_pentomino/",
    "./fireworks/",
    "./rule30/",
  ]

  div_template = <<-EOF
    <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-3">
      <a href="{{folder}}index.html">
        <img src="{{folder}}cart.png"/>
      </a>
    </div>
  EOF

  file_template = nil
  File.open("tic80.template.html") do |fp|
    file_template = fp.read
  end

  content = ""
  carts.each do |cart|
    cmd = "cd #{cart} && tic80 --fs=. --skip --cmd \"load cart.tic & save cart.png & exit\" && cd .."
    `#{cmd}`
    content += div_template.gsub "{{folder}}", cart
  end
  result = file_template.gsub "{{content}}", content
  puts result
end

if __FILE__ == $PROGRAM_NAME
  main
end

