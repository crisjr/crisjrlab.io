-- title:  Studying Conformal Maps
-- author: www.crisjr.eng.br
-- desc:   self-explanatory
-- script: lua

-- ########
-- # DATA #
-- ########
WIDTH=8*30
HEIGHT=8*17
SPRITE=nil

-- #########
-- # MATHS #
-- #########
function twice(z)
	return {
		real=2*z.real,
		complex=2*z.complex,
	}
end

function csin(z)
	local x=z.real
	local y=z.complex
	-- sin(x + yi) = sin(x) cosh(y) + i cos(x) sinh(y)
	return {
		real=math.sin(x)*math.cosh(y),
		complex=math.cos(x)*math.sinh(y),
	}
end

FUNC=csin

function del(f,x)
	local h=0.000001
	return (f(x+h)-f(x-h))/(2*h)
end

function cadd(v,w)
	return {
		real=v.real+w.real,
		complex=v.complex+w.complex,
	}
end

function csub(v,w)
	return {
		real=v.real-w.real,
		complex=v.complex-w.complex,
	}
end

function cmul(v,w)
	local a=v.real
	local b=v.complex
	local c=w.real
	local d=w.complex
	return {
		real=a*c-b*d,
		complex=a*d+b*c,
	}
end

function cdiv(v,w)
	local a=v.real
	local b=v.complex
	local c=w.real
	local d=w.complex
	local p=c*c+d*d
	return {
		real=(a*c+b*d)/p,
		complex=(b*c-a*d)/p,
	}
end

function cdiff(v,w)
	local a=v.real
	local b=v.complex
	local c=w.real
	local d=w.complex
	return ((a-c)^2+(b-d)^2)^0.5
end

function cdel(f,x)
	-- using wirtinger derivatives, on which
	-- z := x + yi
	-- del z = (del x - i del y)/2
	local h=0.000001
	local u={
		real=h,
		complex=h,
	}
	-- (f(x+u)-f(x-u))/(2*u)
	return cdiv(
		csub( -- f(x+h)-f(x-h)
			f(cadd(x,u)),
			f(x)
		),
		u
	)
end

function newton(f,z)
	return csub(
		z,
		cdiv(
			f(z),
			cdel(f,z)
		)
	)
end

function solve_numerically(f,z)
	-- let f(x)=z
	-- this procedures finds out what x produces z for
	-- a given function f
	-- here, we are going to use newton's method
	local e=0.0001
	local x0=z
	local x1=newton(f,z)
	local d=cdiff(x0,x1)

	while d>e do
		x0=x1
		x1=newton(f,x0)
		d=cdiff(x0,x1)
		print("# z = " .. cs(z))
		print("x0 = " .. cs(x0))
		print("f(x0) = " .. cs(f(x0)))
		print("f'(x0) = " .. cs(cdel(f,x0)))
		print("|x0 - x1| = " .. d)
	end

	return x1
end

-- ############
-- # GRAPHICS #
-- ############
function cs(v)
	return "" .. v.real .. " " .. v.complex .. "i"
end

function screen_to_imaginary(i,j)
	local dd=1.0/16
	local r=dd*WIDTH
	local c=dd*HEIGHT
	local u=i*r/WIDTH
	local w=-j*c/HEIGHT
	local real=u
	local complex=w

	return {
		real=real,
		complex=complex,
	}
end

function imaginary_to_conformal_map(x,s)
	local dd=1.0/16
	local r=0
	local c=0
	local v=x.real%1.0
	local w=x.complex%1.0

	if x.real<0.0 then v=-x.real%1.0 end
	if x.complex<0.0 then w=-x.complex%1.0 	end

	for i=0,16 do
		if v>=i*dd and v<(i+1)*dd then
			r=i
		end
		if w>=i*dd and w<(i+1)*dd then
			c=i
		end
	end
	return s[c+1][r+1]
end

-- ##################
-- # MAIN FUNCTIONS #
-- ##################
function setup()
	-- drawing conformal map
	for i=0,WIDTH do for j=0,HEIGHT do
	 -- let f be the function being applied to the
		-- conformal map such that f(x)=z
		-- the goal then is to find out what x is applied
		-- so the previous affirmation is true
		-- discovering x will let us indicate what colour
		-- the conformal map should be at z
		print("---")
		local z=screen_to_imaginary(i,j)
		local x=solve_numerically(FUNC,z)
		-- c=imaginary_to_conformal_map(x,SPRITE)
		print(":: f("..cs(x)..") = "..cs(z))
	end end
end

-- ###############
-- # TIC80 STUFF #
-- ###############
setup()
function TIC()
end
