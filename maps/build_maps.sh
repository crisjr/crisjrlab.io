#!/bin/sh
set -ex

# preparing workspace
rm -rf pre
mkdir pre

# building required files
./build_notes.exe map_data pre
mv pre/index.html .

# cleaning workspace
rm -rf pre

