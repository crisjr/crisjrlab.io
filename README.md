Cris Silva Jr.'s personal website
=================================

[My site](http://www.crisjr.eng.br)'s code.

To clone it:

``` sh
git clone --recursive https://gitlab.com/crisjr/crisjr.gitlab.io crisjr.eng.br 
```

Grateful for the following projects that make it possible:

- [Yahoo's Pure](https://purecss.io/)
- [MathJax](https://www.mathjax.org/)
- [Leaflet](https://leafletjs.com/)
- [Awesome Markers](https://github.com/lvoogdt/Leaflet.awesome-markers)
- [PurgeCSS](https://purgecss.com/)
- [GitLab](https://gitlab.com)
- [qrcode.js](https://davidshimjs.github.io/qrcodejs/)
- [webassembly](https://webassembly.org/)
- [TIC80](http://tic80.com/)
- [bignumber.js](https://github.com/MikeMcl/bignumber.js)
- [Japanese Woodblock Palette](https://lospec.com/palette-list/japanese-woodblock)
- [asdf package manager](https://asdf-vm.com/)
- [lighttpd](https://www.lighttpd.net/)

