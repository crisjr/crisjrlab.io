#!/bin/bash
set -ex

# preparing enviroment notes
sh ./del_html.sh
rm -rf pre
rm -f *.rss

# setup graphics
rm -rf dynamic/web_graphics
cp -r web_graphics dynamic

# building notes
mkdir pre
./build_notes.exe notes pre
mv pre/* .
rm -rf pre
rm -rf notes

cp dynamic/note.html .

