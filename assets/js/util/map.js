const DEFAULT_PLACES = [
    {
        lat: -15.7468,
        lon: -47.8513,
        popup:"Brasília, Distrito Federal",
        status: "home"
    },
    {
        lat: 64.1538,
        lon: -21.9945,
        popup:"Reykjavík, Ísland",
        status: "not visited"
    },
    {
        lat: 36.2709,
        lon: 136.8986,
        popup: "白川、日本",
        status: "visited"
    }
];

const DEFAULT_PINS = {
    "home": {
        icon: 'home',
        markerColor: 'orange',
        iconColor: 'white',
        prefix: 'fa'
    },
    "old_home": {
        icon: 'home',
        markerColor: 'blue',
        iconColor: 'white',
        prefix: 'fa'
    },
    "visited": {
        icon: 'calendar-check',
        markerColor: 'green',
        iconColor: 'white',
        prefix: 'fa'
    },
    "not visited": {
        icon: 'calendar',
        markerColor: 'red',
        iconColor: 'white',
        prefix: 'fa'
    }
};

const DEFAULT_LAT = -18.156;
const DEFAULT_LON = -42.296;
const DEFAULT_ZOOM = 6;

function getUrlParam(key) {
  const urlParams = new URLSearchParams(window.location.search);
  return urlParams.get(key);
}

function loadStuff(defaultOption, urlParam, downloadParam, callback) {
  var urlStuff = getUrlParam(urlParam);
  var downloadStuff = getUrlParam(downloadParam);

  if (!!urlStuff) {
    callback(JSON.parse(decodeURIComponent(urlStuff)));
  } else if (!!downloadStuff) {
    fetch(downloadStuff).then((response) => {
      return response.json();
    }).then((data) => {
      return callback(data);
    }).catch((error) => {
      return callback(defaultOption);
    });
  } else {
    callback(defaultOption);
  }
}

function loadPlaces(callback) {
  return loadStuff(null, "places", "placesurl", callback);
}

function loadPins(callback) {
  return loadStuff(null, "pins", "pinsurl", callback);
}

function loadMapData(callback) {
  return loadStuff({}, "mapdata", "mapdataurl", callback);
}

function pins2icons(pins) {
  var icons = {};

   for (let key in pins) {
     icons[key] = L.AwesomeMarkers.icon(pins[key]);
   }

  return icons;
}

function getFloatParam(mapData, paramKey, defaultValue) {
  return parseFloat(getUrlParam(paramKey) || mapData[paramKey] || defaultValue);
}

function draw(places, pins, initialLatitude, initialLongitude, initialZoom) {
  var icons = pins2icons(pins);
  var mymap = L.map('mapid').setView([initialLatitude, initialLongitude], initialZoom);
  var limit = places.length;

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: 'Leaflet &copy;'
  }).addTo(mymap);

  for (var i = 0; i < limit; i++) {
    var place = places[i];
    var marker = L.marker([place.lat, place.lon], {icon: icons[place.status]});
    marker.addTo(mymap);
    marker.bindPopup(place.popup);
  }
}

/**
 * To customize how this works, set the following URL variables:
 * - places: json object describing places to show
 * - pins: json object describing pins for place markers
 * - placesurl: url to places json object
 * - pinsurl: url to pins json object
 * - lat: initial latitude
 * - lon: initial longitude
 * - zoom: initial zoom
 */
function main() {
  loadMapData((mapData) => {
    loadPlaces((places) => {
      loadPins((pins) => {
        draw(
          places || mapData.places || DEFAULT_PLACES,
          pins || mapData.pins || DEFAULT_PINS,
          getFloatParam(mapData, "lat", DEFAULT_LAT),
          getFloatParam(mapData, "lon", DEFAULT_LON),
          getFloatParam(mapData, "zoom", DEFAULT_ZOOM)
        );
      });
    });
  });
}
