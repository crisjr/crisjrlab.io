/*********
 * MATHS *
 *********/
const ZERO = new BigNumber(0);
const ONE = new BigNumber(1);
const HUNDRED = new BigNumber(100);
const COMISSION_PERCENTAGE = new BigNumber("6.5").div(HUNDRED);
const POSITIVE = ONE;
const NEGATIVE = new BigNumber("-1");

function calculateBet365Green(bet, odd) {
  return bet.times(BigNumber.max(odd.minus(ONE), ZERO));
}

function calculateBet365Red(bet, odd) {
  return bet;
}

function calculateInvertedOdd(odd) {
  return odd.div(odd.minus(1));
}

function calculateBetfairGrossGreen(bet, odd) {
  return bet.times(BigNumber.max(odd.minus(ONE), ZERO));
}

function calculateBetfairLiquidGreen(bet, odd) {
  let result = calculateBetfairGrossGreen(bet, odd);
  let commission = result.times(COMISSION_PERCENTAGE);
  return result.minus(commission);
}

function calculateBetfairRed(bet, odd) {
  return bet;
}

/********
 * VIEW *
 ********/
function s2bn(x) {
  try {
    return new BigNumber(x || "0");
  } catch {
    return ZERO;
  }
}

function bn2s(x) {
  return x.toFixed();
}

function displayArbitrations() {
  var bet365Bet = s2bn(document.getElementById("bet365-bet").value);
  var bet365Odd = s2bn(document.getElementById("bet365-odd").value);
  var betfairBet = s2bn(document.getElementById("betfair-bet").value);
  var betfairIOdd = s2bn(document.getElementById("betfair-iodd").value);
  var bet365Arbitration = calculateBet365Green(bet365Bet, bet365Odd).minus(calculateBetfairRed(betfairBet, betfairIOdd));
  var betfairArbitration = calculateBetfairLiquidGreen(betfairBet, betfairIOdd).minus(calculateBet365Red(bet365Bet, bet365Odd));
  
  document.getElementById("bet365-arbitration").value = bn2s(bet365Arbitration);
  document.getElementById("betfair-arbitration").value = bn2s(betfairArbitration);
}

function updateBetfair() {
  var bet = s2bn(document.getElementById("betfair-bet").value);
  var odd = s2bn(document.getElementById("betfair-odd").value);
  var iodd = calculateInvertedOdd(odd);
  var grossResult = calculateBetfairGrossGreen(bet, iodd);
  var liquidResult = calculateBetfairLiquidGreen(bet, iodd);

  document.getElementById("betfair-iodd").value = bn2s(iodd);
  document.getElementById("betfair-green-gross").value = bn2s(grossResult);
  document.getElementById("betfair-green-liquid").value = bn2s(liquidResult);
  document.getElementById("betfair-red").value = bn2s(calculateBetfairRed(bet, odd));
  displayArbitrations();
}

function updateBet365() {
  var bet = s2bn(document.getElementById("bet365-bet").value);
  var odd = s2bn(document.getElementById("bet365-odd").value);
  var green = calculateBet365Green(bet, odd);

  document.getElementById("bet365-green").value = bn2s(green);
  document.getElementById("bet365-red").value = bn2s(calculateBet365Red(bet, odd));
  document.getElementById("betfair-bet").value = bn2s(green);
  displayArbitrations();
  updateBetfair();
}

function generateUpdateOdd(changeRate, oddElementId, callback) {
  return function() {
    var oddElement = document.getElementById(oddElementId);
    var odd = s2bn(oddElement.value);
    var step = new BigNumber("0.01");  // TODO get step depending on odd
    var newOdd = odd.plus(changeRate.times(step));

    oddElement.value = bn2s(newOdd);

    return callback();
  };
}

function main() {
  document.getElementById("bet365-bet").addEventListener("keyup", updateBet365);
  document.getElementById("bet365-odd").addEventListener("keyup", updateBet365);
  document.getElementById("bet365-odd-up").addEventListener("click", generateUpdateOdd(POSITIVE, "bet365-odd", updateBet365));
  document.getElementById("bet365-odd-down").addEventListener("click", generateUpdateOdd(NEGATIVE, "bet365-odd", updateBet365));
  document.getElementById("betfair-bet").addEventListener("keyup", updateBetfair);
  document.getElementById("betfair-odd").addEventListener("keyup", updateBetfair);
  document.getElementById("betfair-odd-up").addEventListener("click", generateUpdateOdd(POSITIVE, "betfair-odd", updateBetfair));
  document.getElementById("betfair-odd-down").addEventListener("click", generateUpdateOdd(NEGATIVE, "betfair-odd", updateBetfair));
}

