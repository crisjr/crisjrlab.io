var blog = new GithubBlog('ishiikurisu/map-data');
var masterUrl = "https://raw.githubusercontent.com/ishiikurisu/map_data/master/";
blog.loadIndex(function(posts) {
    var content = document.getElementById('content');

    if (posts.error) {
        content.innerHTML = `
        <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-2">
            <h3 class="content-subhead">
                Erro inesperado :(
            </h3>
        </div>
        `;
    } else if (posts.length === 0) {
        content.innerHTML = `
        <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-2">
            <h3 class="content-subhead">
                Está vazio por aqui...
            </h3>
            <p>
                Ainda precisamos de mais mapas...
            </p>
        </div>
        `;
    } else {
        var limit = posts.length;
        var outlet = "";

        for (var i = 0; i < limit; i++) {
            var post = posts[i];
            var mapdataurl = masterUrl + post.path;
            var href = `/util/map?mapdataurl=${mapdataurl}`;
            outlet += `
            <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-2">
                <h3 class="content-subhead">
                    <a class="joe-ribbon-header"
                       href="${href}">
                        ${post.title}
                    </a>
                </h3>
                <p>
                    ${post.description}
                </p>
            </div>
            `;
        }

        content.innerHTML = outlet;
    }
});
