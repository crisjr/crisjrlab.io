function getAllEntries() {
  const dateLength = "2000-01-01".length;
  var children = document.getElementById("content").children;
  var entries = [];

  for (var i = 0; i < children.length; i++) {
    var entry = children[i];
    var creationDateRefIndex = entry.innerHTML.search(/🆕 (.*)/);
    var updatedDateRefIndex = entry.innerHTML.search(/➕ (.*)/); 
    var creationDate = entry.innerHTML.substring(
      creationDateRefIndex + 3,
      creationDateRefIndex + 3 + dateLength
    );
    var lastUpdatedDate = (updatedDateRefIndex < 0)?
      creationDate :
      entry.innerHTML.substring(
        updatedDateRefIndex + 2,
        updatedDateRefIndex + 2 + dateLength
      );

    entries.push({
      element: entry,
      creation_date: creationDate,
      last_updated_date: lastUpdatedDate 
    });
  }

  return entries;
}

function replaceContent(entries) {
  var content = document.getElementById("content");
  content.innerHTML = "";
  
  for (var i = 0; i < entries.length; i++) {
    content.appendChild(entries[i].element);
  }
}

function compareCreatedFirst(a, b) {
  if (a.creation_date < b.creation_date) {
    return -1;
  } else if (a.creation_date > b.creation_date) {
    return 1;
  } else {
    return 0;
  }
}

function compareCreatedLast(a, b) {
  if (a.creation_date > b.creation_date) {
    return -1;
  } else if (a.creation_date < b.creation_date) {
    return 1;
  } else {
    return 0;
  }
}

function compareUpdatedFirst(a, b) {
  if (a.last_updated_date < b.last_updated_date) {
    return -1;
  } else if (a.last_updated_date > b.last_updated_date) {
    return 1;
  } else {
    return 0;
  }
}

function compareUpdatedLast(a, b) {
  if (a.last_updated_date > b.last_updated_date) {
    return -1;
  } else if (a.last_updated_date < b.last_updated_date) {
    return 1;
  } else {
    return 0;
  }
}

function main() {
  document.getElementById("controls").classList.remove("disabled");
  document.getElementById("created-first").addEventListener("click", () => {
    replaceContent(getAllEntries().sort(compareCreatedFirst));
  });
  document.getElementById("created-last").addEventListener("click", () => {
    replaceContent(getAllEntries().sort(compareCreatedLast));
  });
  document.getElementById("updated-first").addEventListener("click", () => {
    replaceContent(getAllEntries().sort(compareUpdatedFirst));
  });
  document.getElementById("updated-last").addEventListener("click", () => {
    replaceContent(getAllEntries().sort(compareUpdatedLast));
  });
}
