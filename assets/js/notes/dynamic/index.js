var blog = new GithubBlog('ishiikurisu/notes');

function formatDate(raw) {
    const dt = new Date(raw);
    console.log(dt);
    const year = dt.getFullYear();
    const month = 1 + dt.getMonth();
    const date = dt.getDate();
    return `${year}-${month}-${date}`;
}

blog.loadIndex(function(posts) {
    var content = document.getElementById('content');

    if (posts.error) {
        content.innerHTML = `
        <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-2">
            <h3 class="content-subhead">
                Erro inesperado :(
            </h3>
            <p>
                Tente novamente mais tarde ou veja a versão <a href="/notes" class="joe-ribbon">versão estática</a>.
            </p>
        </div>
        `;
    } else if (posts.length === 0) {
        content.innerHTML = `
        <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-2">
            <h3 class="content-subhead">
                Está vazio por aqui...
            </h3>
            <p>
                Ainda precisamos escrever mais textos antes de postar aqui. Continue de olho!
            </p>
        </div>
        `;
    } else {
        var limit = posts.length;
        var outlet = "";

        for (var i = 0; i < limit; i++) {
            var post = posts[i];
            var dateLabel = "";

            if (!!post.creation_date) {
                dateLabel = "<p>🆕 " + formatDate(post.creation_date);

                if (post.creation_date !== post.last_updated_date) {
                    dateLabel += " ➕ " + formatDate(post.last_updated_date);
                }

                dateLabel += "</p>";
            }

            outlet += `
            <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-2">
                <h3 class="content-subhead">
                    <a class="joe-ribbon-header"
                       href="./note.html?which=${post.path}">
                        ${post.title}
                    </a>
                </h3>
                <p>
                    ${post.description}
                </p>
                ${dateLabel}
            </div>
            `;
        }

        content.innerHTML = outlet;
    }
});
