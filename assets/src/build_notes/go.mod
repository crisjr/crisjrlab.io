module crisjr.eng.br/notes/build_notes

go 1.23.2

require (
	git.sr.ht/~m15o/gmi2html v0.4.0 // indirect
	github.com/gomarkdown/markdown v0.0.0-20240930133441-72d49d9543d8 // indirect
)
