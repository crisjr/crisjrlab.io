/* #############
   # FUNCTIONS #
   ############# */

function loadCypher() {
    const rawCypher = document.getElementById('cypher-key').value;
    const lines = rawCypher.split('\n');
    var outlet = { };

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];
	if (line.length > 0) {
	    var fields = line.split(' ');
	    var key = fields[0];
	    var value = fields[1];
	    outlet[key] = value;
	}
    }

    return outlet;
}

function invert(inlet) {
    var outlet = {};
    
    for (const key in inlet) {
        outlet[inlet[key]] = key;
    }

    return outlet;
}

function hide(inlet, cypher) {
    var outlet = '';

    for (var i = 0; i < inlet.length; i++) {
        var key = inlet[i];
        var value = cypher[key];
	outlet += (!!value)? value : key;
    }

    return outlet;
}

function uncover(inlet, cypher) {
    const inverted_cypher = invert(cypher);
    return hide(inlet, inverted_cypher);
}

/* #########
   # SETUP #
   ######### */

document.getElementById('hide-button').addEventListener('click', function() {
    const inlet = document.getElementById('original-text').value;
    const cypher = loadCypher();
    const outlet = hide(inlet, cypher);
    document.getElementById('hidden-text').value = outlet;
});

document.getElementById('uncover-button').addEventListener('click', function() {
    const inlet = document.getElementById('hidden-text').value;
    const cypher = loadCypher();
    const outlet = uncover(inlet, cypher);
    document.getElementById('original-text').value = outlet;
});

document.getElementById('cypher-key').value = `a а
b б
c ж
d д
e е
f ф
g г
h х
i и
j ц
k к
l л
m м
n н
o о
p п
q щ
r р
s с
t т
u у
v в
w у
x ь
y я
z з`;

